//
//  GameOverScene.swift
//  JHero Space
//
//  Created by Jamal on 6/30/19.
//  Copyright © 2019 Jamal. All rights reserved.
//

import Foundation
import SpriteKit

class GameOverScene: SKScene {
    
    var restartLabel = SKLabelNode()
    
    func createLabel(fontSize: CGFloat = 125, text: String, position: CGPoint)-> SKLabelNode{
        
        let label = SKLabelNode(fontNamed: "Chalkduster")
        label.text = text
        label.fontSize = fontSize
        label.fontColor = .white
        label.position = position
        label.zPosition = 1
        
        addChild(label)
        return label
    }
    
    func positionRatio(dx: CGFloat, dy: CGFloat)-> CGPoint{
        return CGPoint(x: size.width * dx, y: size.height * dy)
    }
    
    override func didMove(to view: SKView) {
        
        let back = SKSpriteNode(imageNamed: "background")
        back.position = positionRatio(dx: 0.5, dy: 0.5)
        back.zPosition = 0
        addChild(back)
        
        let _ = createLabel(fontSize: 170, text: "Game Over",
                    position: positionRatio(dx: 0.5, dy: 0.7))
        let _ = createLabel(text: "score: \(gameScore)",
            position: positionRatio(dx: 0.5, dy: 0.55))
        
        var highScore = UserDefaults.standard.integer(forKey: "highScore")
        
        if gameScore > highScore{
            highScore = gameScore
            UserDefaults.standard.set(highScore, forKey: "highScore")
        }
        
        let _ = createLabel(text: "High Score: \(highScore)", position: positionRatio(dx: 0.5, dy: 0.45))
        
        restartLabel = createLabel(fontSize: 90, text: "restart", position: positionRatio(dx: 0.5, dy: 0.3))
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches{
            
            let point = touch.location(in: self)
            
            if restartLabel.contains(point){
                
                let scene = GameScene(size: size)
                scene.scaleMode = scaleMode
                let transition = SKTransition.fade(withDuration: 0.5)
                view?.presentScene(scene, transition: transition)
            }
        }
    }
    
}
