//
//  GameScene.swift
//  JHero Space
//
//  Created by Jamal on 6/25/19.
//  Copyright © 2019 Jamal. All rights reserved.
//

import SpriteKit
import GameplayKit

var gameScore = 0
class GameScene: SKScene {
    
    
    var actionKey = "spawningEnemies"
    
    var livesNumber = 3 {
        didSet{
            livesLabel.text = "Lives: \(livesNumber)"
        }
    }
    var livesLabel = SKLabelNode()
    
    var levelNumber = 0
    var scoreLabel = SKLabelNode()
    
    let player = SKSpriteNode(imageNamed: "playerShip")
    
    let bulletSound = SKAction.playSoundFileNamed("bulletSound.mp3", waitForCompletion: false)
    let exploionVoice = SKAction.playSoundFileNamed("exploionVoice.m4a", waitForCompletion: false)
    
    var tapToStartLabel = SKLabelNode(fontNamed: "Chalkduster")
    
    var score = 0 {
        didSet{
            scoreLabel.text = "Score: \(score)"
            gameScore = score
        }
    }
    
    enum gameState{
        case preGame
        case inGame
        case afterGame
    }
    
    var state: gameState = .preGame
    
    struct PhysicsCategories {
        static let none: UInt32 = 0
        static let player: UInt32 = 0b1 //1
        static let bullet: UInt32 = 0b10//2
        static let enemy: UInt32 = 0b100//4
    }
    
    func random() -> CGFloat {
        return CGFloat(Float(arc4random()) / 0xFFFFFFFF)
    }
    func random(min: CGFloat, max: CGFloat) -> CGFloat {
        return random() * (max - min) + min
    }
    
    
    var gameArea: CGRect
    
    override init(size: CGSize) {
        
        let maxAspectRatio: CGFloat = 16.0/9.0
        let playableWidth = size.height / maxAspectRatio
        let margin = (size.width - playableWidth) / 2
        gameArea = CGRect(x: margin, y: 0, width: playableWidth, height: size.height)
        
        super.init(size: size)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMove(to view: SKView) {
        score = 0
        physicsWorld.contactDelegate = self
        
        for i in 0...1{
            
            let background = SKSpriteNode(imageNamed: "background")
            background.size = self.size
            background.anchorPoint = CGPoint(x: 0.5, y: 0)
            background.position = CGPoint(x: size.width/2, y: size.height * CGFloat(i))
            background.zPosition = 0
            background.name = "background"
            addChild(background)
            
        }
        
        player.setScale(1)
        player.position = CGPoint(x: size.width/2, y: -player.size.height)
        player.zPosition = 2
        player.physicsBody = SKPhysicsBody(rectangleOf: player.size)
        player.physicsBody?.affectedByGravity = false
        player.physicsBody?.categoryBitMask = PhysicsCategories.player
        player.physicsBody?.collisionBitMask = PhysicsCategories.none
        player.physicsBody?.contactTestBitMask = PhysicsCategories.enemy
        
        addChild(player)
        
        scoreLabel.fontName = "duckduster"
        scoreLabel.fontSize = 70
        scoreLabel.fontColor = SKColor.white
        scoreLabel.horizontalAlignmentMode = .left
        scoreLabel.position = CGPoint(x: size.width*0.15, y: size.height + scoreLabel.frame.size.height)
        scoreLabel.zPosition = 100
        addChild(scoreLabel)
        
        livesLabel.fontName = "duckduster"
        livesLabel.fontSize = 70
        livesLabel.fontColor = SKColor.white
        livesLabel.horizontalAlignmentMode = .right
        livesLabel.position = CGPoint(x: size.width*0.85, y: size.height + livesLabel.frame.size.height)
        livesLabel.zPosition = 100
        addChild(livesLabel)
        score = 0
        
        let moveAction = SKAction.moveTo(y: size.height * 0.9, duration: 0.3)
        scoreLabel.run(moveAction)
        livesLabel.run(moveAction)
        
        
        tapToStartLabel.text = "Tap to Start"
        tapToStartLabel.fontSize = 100
        tapToStartLabel.fontColor = .white
        tapToStartLabel.position = CGPoint(x: size.width/2, y: size.height/2)
        tapToStartLabel.zPosition = 1
        tapToStartLabel.alpha = 1
        addChild(tapToStartLabel)
        
        tapToStartLabel.run(SKAction.fadeIn(withDuration: 0.3))
        
        
    }
    
    func startGame(){
        
        state = .inGame
        tapToStartLabel.run(SKAction.sequence([
            SKAction.fadeOut(withDuration: 0.5),
            SKAction.removeFromParent()
            ]))
        
        player.run(SKAction.sequence([
            SKAction.moveTo(y: size.height * 0.2, duration: 0.5),
            SKAction.run {self.startNewLevel()}
            ]))
        
        
    }
    
    func changeLife(){
        
        livesNumber -= 1
        
        livesLabel.run(SKAction.sequence([
            SKAction.scale(to: 1.5, duration: 0.2),
            SKAction.scale(to: 1, duration: 0.2),
            ]))
        
        if livesNumber == 0{
            gameOver()
        }
    }
    
    func addScore(){
        score += 1
        
        if score == 10 || score == 25 || score == 50 {
            startNewLevel()
        }
    }
    
    
    
    func spawnExplosion(_ point: CGPoint) {
        
        let explosion = SKSpriteNode(imageNamed: "explosition")
        explosion.position = point
        explosion.zPosition = 3
        explosion.setScale(0)
        addChild(explosion)
        
        explosion.run(SKAction.sequence([
            exploionVoice,
            SKAction.scale(to: 1, duration: 0.1),
            SKAction.fadeOut(withDuration: 0.1),
            SKAction.removeFromParent()
            ]))
    }
    
    
    func startNewLevel(){
        
        levelNumber += 1
        
        if action(forKey: actionKey) != nil{
            removeAction(forKey: actionKey)
        }
        
        var levelDuration = TimeInterval()
        
        switch levelNumber {
        case 1: levelDuration = 1.2
        case 2: levelDuration = 1
        case 3: levelDuration = 0.8
        case 4: levelDuration = 0.5
        default:
            levelDuration = 0.5
            print("Cannot find level info")
        }
        
        run(SKAction.repeatForever(SKAction.sequence([
            SKAction.run(spawnTargets),
            SKAction.wait(forDuration: levelDuration)
            ])), withKey: actionKey)
    }
    
    func fireBullet(){
        
        let bullet = SKSpriteNode(imageNamed: "bullet")
        bullet.name = "Bullet"
        bullet.setScale(1)
        bullet.position = player.position
        bullet.zPosition = 1
        bullet.physicsBody = SKPhysicsBody(rectangleOf: bullet.size)
        bullet.physicsBody?.affectedByGravity = false
        bullet.physicsBody?.categoryBitMask = PhysicsCategories.bullet
        bullet.physicsBody?.collisionBitMask = PhysicsCategories.none
        bullet.physicsBody?.contactTestBitMask = PhysicsCategories.enemy
        addChild(bullet)
        
        let moveBullet = SKAction.moveTo(y: size.height + bullet.size.height, duration: 1)
        let deleteBullet = SKAction.removeFromParent()
        let bulletSequnce = SKAction.sequence([bulletSound ,moveBullet, deleteBullet])
        bullet.run(bulletSequnce)
        
    }
    
    func gameOver(){
        
        state = .afterGame
        
        removeAllActions()
        
        enumerateChildNodes(withName: "Bullet"){bullet, _ in
            bullet.removeAllActions()
        }
        
        enumerateChildNodes(withName: "Target"){target, _ in
            target.removeAllActions()
        }
        
        run(SKAction.sequence([
            SKAction.wait(forDuration: 1),
            SKAction.run {self.changeScene()}
            ]))
        
    }
    
    func changeScene(){
        let scene = GameOverScene(size: size)
        scene.scaleMode = self.scaleMode
        view?.presentScene(scene, transition: SKTransition.fade(withDuration: 0.5))
    }
    
    func spawnTargets(){
        
        let randomXStart = random(min: gameArea.minX, max: gameArea.maxX)
        let randomXEnd = random(min: gameArea.minX, max: gameArea.maxX)
        
        let startPoint = CGPoint(x: randomXStart, y: size.height * 1.2)
        let endPoint = CGPoint(x: randomXEnd, y: -size.height * 0.2)
        
        let target = SKSpriteNode(imageNamed: "enemyShip")
        target.setScale(1)
        target.name = "target"
        target.position = startPoint
        target.zPosition = 2
        target.physicsBody = SKPhysicsBody(rectangleOf: target.size)
        target.physicsBody?.affectedByGravity = false
        target.physicsBody?.categoryBitMask = PhysicsCategories.enemy
        target.physicsBody?.collisionBitMask = PhysicsCategories.none
        target.physicsBody?.contactTestBitMask = PhysicsCategories.bullet | PhysicsCategories.player
        addChild(target)
        
        if state == .inGame{
            target.run(SKAction.sequence([
                SKAction.move(to: endPoint, duration: 1.5),
                SKAction.removeFromParent(),
                SKAction.run {self.changeLife()}
                ]))
        }
        
        let dx = endPoint.x - startPoint.x
        let dy = endPoint.y - startPoint.y
        let amountToRotate = atan2(dy, dx)
        target.zRotation = amountToRotate
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if state == .preGame{
            startGame()
        }
            
        else if state == .inGame{
            fireBullet()
        }
        
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        for touch in touches{
            
            let pointOfTouch = touch.location(in: self)
            let previousPointOfTouch = touch.previousLocation(in: self)
            
            let amountDragged = pointOfTouch.x - previousPointOfTouch.x
            
            if state == .inGame{
                player.position.x += amountDragged
            }
            
            if player.position.x > gameArea.maxX - player.size.width / 2{
                player.position.x = gameArea.maxX - player.size.width / 2
            }
            
            if player.position.x < gameArea.minX + player.size.width / 2 {
                player.position.x = gameArea.minX + player.size.width / 2
            }
            
        }
    }
    
    var lastUpdateTime: TimeInterval = 0
    var deltaFrameTime: TimeInterval = 0
    var movePerSecount: CGFloat = 600
    
    override func update(_ currentTime: TimeInterval) {
        
        if lastUpdateTime == 0{
            lastUpdateTime = currentTime
        }
        else{
            deltaFrameTime = currentTime - lastUpdateTime
            lastUpdateTime = currentTime
        }
        
        let moveAmount = movePerSecount * CGFloat(deltaFrameTime)
        
        enumerateChildNodes(withName: "background"){ background, _ in
            
            if self.state == .inGame{
                background.position.y -= moveAmount
            }
            
            if background.position.y < -self.size.height{
                background.position.y += self.size.height * 2
            }
        }
    }
    
}

extension GameScene: SKPhysicsContactDelegate{
    
    func didBegin(_ contact: SKPhysicsContact) {
        
        var body1 = contact.bodyA
        var body2 = contact.bodyB
        
        if contact.bodyA.categoryBitMask > contact.bodyB.categoryBitMask{
            body1 = contact.bodyB
            body2 = contact.bodyA
        }
        
        if body1.categoryBitMask == PhysicsCategories.player && body2.categoryBitMask == PhysicsCategories.enemy{
            // if player has hit the enemy
            if body1.node != nil{
                spawnExplosion(body1.node!.position)
            }
            
            if body1.node != nil{
                spawnExplosion(body2.node!.position)
            }
            
            body1.node?.removeFromParent()
            body2.node?.removeFromParent()
            
            gameOver()
        }
        
        if body1.categoryBitMask == PhysicsCategories.bullet && body2.categoryBitMask == PhysicsCategories.enemy && (body2.node?.position.y)! < size.height {
            // if the bullet has hit enemy
            
            addScore()
            
            if body2.node != nil{
                spawnExplosion(body2.node!.position)
            }
            body1.node?.removeFromParent()
            body2.node?.removeFromParent()
        }
    }
    
    
}
