//
//  GameViewController.swift
//  JHero Space
//
//  Created by Jamal on 6/25/19.
//  Copyright © 2019 Jamal. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit
import AVFoundation

class GameViewController: UIViewController {

    var audio = AVAudioPlayer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let path = Bundle.main.path(forResource: "back", ofType: "mp3")!
        let url = URL(fileURLWithPath: path)
        
        
        
        do{ audio = try AVAudioPlayer(contentsOf: url)}
        catch{return print("audion cannot find")}
        
        audio.numberOfLoops = -1
        audio.play()
        
        
        if let view = self.view as! SKView? {
            
            let scene = MainScene(size: CGSize(width: 1536, height: 2048))
            
                scene.scaleMode = .aspectFill
                view.presentScene(scene)
            
            
            view.ignoresSiblingOrder = true
            
            view.showsFPS = false
            view.showsNodeCount = false
        }
    }

    override var shouldAutorotate: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
}
