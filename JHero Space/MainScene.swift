//
//  MainScene.swift
//  JHero Space
//
//  Created by Jamal on 7/16/19.
//  Copyright © 2019 Jamal. All rights reserved.
//

import Foundation
import SpriteKit

class MainScene: SKScene{
    
    func createLabel(fontSize: CGFloat = 125, text: String, position: CGPoint)-> SKLabelNode{
        
        let label = SKLabelNode(fontNamed: "Chalkduster")
        label.text = text
        label.fontSize = fontSize
        label.fontColor = .white
        label.position = position
        label.zPosition = 1
        
        addChild(label)
        return label
    }
    
    
    func positionRatio(dx: CGFloat, dy: CGFloat)-> CGPoint{
        return CGPoint(x: size.width * dx, y: size.height * dy)
    }
    
    override func didMove(to view: SKView) {
        
        let back = SKSpriteNode(imageNamed: "background")
        back.position = positionRatio(dx: 0.5, dy: 0.5)
        back.zPosition = 0
        addChild(back)
        
        let _ = createLabel(fontSize: 50, text: "Jamalzare",
            position: positionRatio(dx: 0.5, dy: 0.78))
        
        let _ = createLabel(fontSize: 200, text: "JHero",
                            position: positionRatio(dx: 0.5, dy: 0.7))
        
        let _ = createLabel(fontSize: 200, text: "Space",
                            position: positionRatio(dx: 0.5, dy: 0.625))
        
        let startLabel = createLabel(fontSize: 150, text: "Start Game",
                            position: positionRatio(dx: 0.5, dy: 0.4))
        startLabel.name = "startButton"
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches{
            
            let point = touch.location(in: self)
            
            let tappedNode = atPoint(point)
            if tappedNode.name == "startButton"{
                let scene = GameScene(size: size)
                scene.scaleMode = scaleMode
                let transition = SKTransition.fade(withDuration: 0.5)
                view?.presentScene(scene, transition: transition)
            }
            
        }
    }
}
